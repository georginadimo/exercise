package com.company;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Scanner;

        public class Main {


            public static void main(String[] args) throws IOException {
                // write your code
                ArrayList<String> list= new ArrayList<String>();
                System.out.println("Enter the path of the file");
                Scanner input = new Scanner(System.in);
                String path = input.nextLine();

                Scanner scanner = new Scanner(System.in);
                boolean validData = true;
                String word = "";
                while (validData) {
                    System.out.print("Please enter a word, enter -1 when done: ");

                    try {
                        word = scanner.next();
                    }catch (InputMismatchException ex){
                        System.out.println(ex.getMessage());
                    }

                    if(word.compareToIgnoreCase( " ") == 0 || word.compareToIgnoreCase("-1") == 0){
                        validData = false;
                    }else{
                        list.add(word);
                        System.out.printf("Your word was %s%n", word);
                    }

                }
                File file = new File(path);
                String file_type=getFileExtension(file);
                System.out.println(file_type);

                SearchResults search = new SearchResults();
                if (file_type.equals("pdf"))
                {
                    PDFTextFinder pdftext = new PDFTextFinder();
                    ArrayList results_pdf=pdftext.findTextInPDFDocument(path,list);
                    search.getResults(results_pdf);
                }
                else
                {
                    MSWordTextFinder doctext = new MSWordTextFinder();
                    ArrayList results_word=doctext.findTextInWordDocument(path,list);
                    search.getResults(results_word);

                }

    }
    private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
            return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "";
    }
}
